<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>潜在客户</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function controlUpdateBtn(stateResult){

}
var orgIdBox;
function openProIdBox(){
	bachProcessIds();
	var ids = $("#ids").val();
	if(ids == ""){
		writeErrorMsg('请先选中一条记录!');
	}else{
		var handlerId = "SalerListSelectList"; 
		if (!orgIdBox){
			orgIdBox = new PopupBox('orgIdBox','请选择人员',{size:'normal',width:'300',top:'2px'});
		}
		var url = 'index?'+handlerId+'&ids='+ids+'&targetId=ORG_SALESMAN';
		orgIdBox.sendRequest(url);
	}
}
function doBatchDelete(){
	bachProcessIds();
	var ids = $("#ids").val();
	if(ids == ""){
		writeErrorMsg('请先选中一条记录!');
	}else{
		jConfirm(confirmMsg,function(r){
			if(r){
				var url = "<%=pageBean.getHandlerURL()%>&actionType=batchDelete&ids="+$("#ids").val();
				sendRequest(url,{onComplete:function(responseText){
					if(responseText != ""){
						jAlert(responseText);
						doSubmit({actionType:'prepareDisplay'});
					}else{
						doSubmit({actionType:'prepareDisplay'});
					}
				}});
			}
		});
	}
}
function bachProcessIds(){
	var ids = "";
	$("input[name = 'ORG_ID']:checked").each(function(){   
		ids = ids+$(this).val()+",";
	});
	if (ids.length > 0){
		ids = ids.substring(0,ids.length-1);
	}
	$("#ids").val(ids);
}
function assignedSale(){
	$('#ORG_SALESMAN').val();
	doSubmit({actionType:'assignedSale'});
}

var importRequestBox;
function importRequest(){
	if (!importRequestBox){
		importRequestBox = new PopupBox('importRequestBox','潜在客户导入',{size:'big',height:'600px',width:'1050',top:'3px'});
	}
	var url = 'index?ProcustUpload';
	importRequestBox.sendRequest(url);
}
function refresh(){
	doSubmit({actionType:'prepareDisplay'});
}
function selectedCheck(indexId){	
	var idInt = parseInt(indexId);
	var currentIndexId = idInt ;
	if($("#ec_table tr:eq("+currentIndexId+") input[name = 'ORG_ID']").is(':checked')){
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'ORG_ID']").attr('checked',false);
	}else{
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'ORG_ID']").attr('checked',true);
	}
}
function showDetail(actionType,orgId) {
	var ids = '';
	if(!orgId){
		bachProcessIds();
		ids = $("#ids").val();
		if(ids.length > 36){
			writeErrorMsg('只能选中一条记录!');
			return;
		}
		if (ids == ""){
			writeErrorMsg('请先选中一条记录!');
			return;
		}
	}else{
		ids = orgId;
	}
	var orgState = $('#orgState').val();
	var sdate = $('#sdate').val();
	var edate = $('#edate').val();
	var orgName = $('#orgName').val();
	var orgClassification = $('#orgClassification').val();
	var orgLabels = $('#orgLabels').val();
	var orgSalesman = $('#orgSalesman').val();
	
	var url = "/index?OrgInfoManageList&actionType="+actionType+"&ORG_ID="+ids+"&orgState="+orgState+"&sdate="+sdate+"&edate="+edate+"&orgName="+orgName+"&orgClassification="+orgClassification+"&orgLabels="+orgLabels+"&orgSalesman="+orgSalesman;
	var path = "<%=request.getContextPath() %>";
	url = path + url;
	window.location.href = url;
}
function showUpdate(actionType) {
	bachProcessIds();
	var ids = $("#ids").val();
	if(ids.length > 36){
		writeErrorMsg('只能选中一条记录!');
		return;
	}
	if (ids == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	var url = "/index?OrgInfoManageEdit&operaType=update&ORG_ID="+ids;
	var path = "<%=request.getContextPath() %>";
	url = path + url;
	window.location.href = url;
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <aeai:previlege code="create"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td></aeai:previlege>
   <aeai:previlege code="edit"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="showUpdate('updateRequest')"><input id="editImgBtn" value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td></aeai:previlege>
   <aeai:previlege code="detail"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="showDetail('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td></aeai:previlege> 
   <aeai:previlege code="delete"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doBatchDelete()"><input id="delImgBtn" value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td></aeai:previlege>
   <aeai:previlege code="assign"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="openProIdBox()"><input value="&nbsp;" type="button" class="assignImgBtn" id="assignImgBtn" title="分配" />分配</td></aeai:previlege>
   <aeai:previlege code="excel"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="importRequest()"><input value="&nbsp;" type="button" class="excelImgBtn" id="excelImgBtn" title="导入" />导入</td></aeai:previlege>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;状态<select id="orgState" label="状态" name="orgState" class="select" onchange="doQuery()"><%=pageBean.selectValue("orgState")%></select>
&nbsp;更新日期<input id="sdate" label="开始日期" name="sdate" type="text" value="<%=pageBean.inputDate("sdate")%>" size="8" class="text" readonly="readonly"/><img id="sdatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
-<input id="edate" label="截止日期" name="edate" type="text" value="<%=pageBean.inputDate("edate")%>" size="8" class="text"  readonly="readonly"/><img id="edatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
&nbsp;客户名称<input id="orgName" label="客户名称" name="orgName" type="text" value="<%=pageBean.inputValue("orgName")%>" size="15" class="text" ondblclick="emptyText('orgName')" />
&nbsp;分类<select id="orgClassification" label="分类" name="orgClassification"  class="select" onchange="doQuery()"><%=pageBean.selectValue("orgClassification")%></select>
&nbsp;标签<select id="orgLabels" label="标签" name="orgLabels" class="select" onchange="doQuery()"><%=pageBean.selectValue("orgLabels")%></select>
<%if(pageBean.getBoolValue("doOpera")){ %>
&nbsp;跟进人员<select id="orgSalesman" label="跟进人员" name="orgSalesman" class="select" onchange="doQuery()"><%=pageBean.selectValue("orgSalesman")%></select>
<%} %>
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" 
retrieveRowsCallback="process" 
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |extend|status"
width="100%" rowsDisplayed="${ec_rd == null ?15:ec_rd}"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();showDetail('viewDetail','${row.ORG_ID}');" oncontextmenu="selectRow(this,{ORG_ID:'${row.ORG_ID}'});controlUpdateBtn('${row.ORG_STATE}');refreshConextmenu()" onclick="selectedCheck('${GLOBALROWCOUNT}');selectRow(this,{ORG_ID:'${row.ORG_ID}'});controlUpdateBtn('${row.ORG_STATE}');">
	<ec:column width="25" style="text-align:center" property="ORG_ID" cell="checkbox" headerCell="checkbox" onclick="selectedCheck('${GLOBALROWCOUNT}');"/>
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="120" property="ORG_NAME" title="客户名称" />
	<ec:column width="80" property="ORG_CLASSIFICATION" title="分类" mappingItem="ORG_CLASSIFICATION"/>
	<ec:column width="100" property="ORG_LINKMAN_NAME" title="联系人" />
	<ec:column width="50" property="ORG_STATE" title="状态" mappingItem="ORG_STATE"/>
	<ec:column width="80" property="ORG_SALESMAN_NAME" title="跟进人员" />
    <ec:column width="100" property="ORG_UPDATE_TIME" title="更新时间" />
	<ec:column width="120" property="ORG_LABELS_NAME" title="标签" />
</ec:row>
</ec:table>
<input type="hidden" name="ORG_ID" id="ORG_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="ORG_SALESMAN" id="ORG_SALESMAN" value="<%=pageBean.inputValue("ORG_SALESMAN")%>"/>
<input type="hidden" id="ids" name="ids" value="<%=pageBean.inputValue("ids")%>" />
<script language="JavaScript">
setRsIdTag('ORG_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
initCalendar('sdate','%Y-%m-%d','sdatePicker');
initCalendar('edate','%Y-%m-%d','edatePicker');
datetimeValidators[0].set("yyyy-MM-dd").add("sdate");
datetimeValidators[0].set("yyyy-MM-dd").add("edate");
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
