<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<%@ taglib uri="http://www.agileai.com" prefix="aeai"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>用户列表</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>  
   <aeai:previlege code="refresh"><td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="R" align="center" onclick="doSubmit({actionType:'prepareDisplay'})"><input value="&nbsp;" type="button" class="cancelImgBtn" id="cancelImgBtn" title="刷新" />刷新</td></aeai:previlege>   
</tr>
</table>
</div>

<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="80" nowrap>拜访次数</th>
	<td><input name="STATIS_VISIT" class="text" id="STATIS_VISIT"  value="<%=pageBean.inputValue("STATIS_VISIT")%>" size="18" readonly="readonly" label="拜访个数" /></td>
</tr>
<tr>
	<th width="80" nowrap style="padding-left:20px">-预约拜访</th>
	<td><input name="STATIS_APPOINTMENT_VISIT" class="text" id="STATIS_APPOINTMENT_VISIT"  value="<%=pageBean.inputValue("STATIS_APPOINTMENT_VISIT")%>" size="18" readonly="readonly" label="预约拜访" /></td>
</tr>
<tr>
	<th width="80" nowrap style="padding-left:20px">-电话拜访</th>
	<td><input name="STATIS_PHONE_VISIT" class="text" id="STATIS_PHONE_VISIT"  value="<%=pageBean.inputValue("STATIS_PHONE_VISIT")%>" size="18" readonly="readonly" label="预约拜访" /></td>
</tr>
<tr>
	<th width="80" nowrap style="padding-left:20px">-邮件拜访</th>
	<td><input name="STATIS_EMAIL_VISIT" class="text" id="STATIS_EMAIL_VISIT"  value="<%=pageBean.inputValue("STATIS_EMAIL_VISIT")%>" size="18" readonly="readonly" label="预约拜访" /></td>
</tr>
<tr>
	<th width="80" nowrap style="padding-left:20px">-陌生拜访</th>
	<td><input name="STATIS_STRANGE_VISIT" class="text" id="STATIS_STRANGE_VISIT"  value="<%=pageBean.inputValue("STATIS_STRANGE_VISIT")%>" size="18" readonly="readonly" label="预约拜访" /></td>
</tr>
<tr>
<th width="80" nowrap>商机个数</th>
	<td><input name="STATIS_OPP" class="text" id="STATIS_OPP"  value="<%=pageBean.inputValue("STATIS_OPP")%>" size="18" readonly="readonly" label="商机个数" /></td>
</tr>
<tr>
<th width="80" nowrap>订单个数</th>
	<td><input name="STATIS_ORDER" class="text" id="STATIS_ORDER"  value="<%=pageBean.inputValue("STATIS_ORDER")%>" size="18" readonly="readonly" label="订单个数" /></td>
</tr>   
</table>
<input type="hidden" id="STATIS_ID" name="STATIS_ID" value="<%=pageBean.inputValue("STATIS_ID")%>" />
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" id="custId" name="custId" value="<%=pageBean.inputValue("custId")%>" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
