package com.agileai.crm.module.procustomer.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeSelectService;

public interface LabelsTreeSelect
        extends TreeSelectService {
	public List<DataRow> queryPerTreeRecords(DataParam param);
}
